FROM jenkins/jenkins:lts
MAINTAINER stephen-njoroge <steven.n360@gmail.com>
USER root

ARG DOCKER_COMPOSE=1.6.2
ARG DOCKER_ENGINE=18.06.1~ce~3-0~debian
# Install the latest Docker CE binaries
RUN apt-get update && \
    apt-get -y install apt-transport-https \
      ca-certificates \
      curl \
      gnupg2 \
      apt-transport-https python-dev \
      python-setuptools \
      gcc make libssl-dev -y \
      software-properties-common && \
    curl -fsSL https://download.docker.com/linux/$(. /etc/os-release; echo "$ID")/gpg > /tmp/dkey; apt-key add /tmp/dkey && \
    add-apt-repository \
      "deb [arch=amd64] https://download.docker.com/linux/$(. /etc/os-release; echo "$ID") \
      $(lsb_release -cs) \
      stable" && \
    apt-get update && \
    # run the command below to know exact verisons of docker-ce available for install
    # put your desired version on the DOCKER_ENGINE ARG command
    #apt-cache madison docker-ce && \
    apt-get -y install docker-ce=${DOCKER_ENGINE} && \
    easy_install pip

RUN pip install docker-compose==${DOCKER_COMPOSE:-1.6.2} && pip install ansible boto boto3


# Add Jenkins plugins
COPY plugins.txt /usr/share/jenkins/ref/plugins.txt
RUN /usr/local/bin/install-plugins.sh < /usr/share/jenkins/ref/plugins.txt